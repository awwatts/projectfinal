<?php

global $pdo;

$ip_address = 'mysql-db'; // Remplacez ceci par l'adresse IP de votre serveur MySQL ou mysql-db en docker compose
$port = '3306'; // Remplacez ceci par le port MySQL approprié
$user = 'root';
$password = 'root';
$dbname = 'db';

try {
    $pdo = new PDO("mysql:host=$ip_address;port=$port;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Failed to connect to MySQL: " . $e->getMessage();
    die();
}