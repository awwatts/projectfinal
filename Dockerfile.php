FROM php:7.4-apache

# Installez le pilote PDO MySQL
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mysqli

WORKDIR /var/www/html

COPY ./ /var/www/html

EXPOSE 80
 